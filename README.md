# Zaaksysteem Tools Library

[![build status](https://gitlab.com/zaaksysteem/zaaksysteem-tools/badges/master/build.svg)](https://gitlab.com/zaaksysteem/zaaksysteem-tools/commits/master)

This repository hosts common utilities used in the Zaaksysteem projects
