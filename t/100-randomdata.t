use Test::More;

use Zaaksysteem::Tools::RandomData qw(:all);
use Zaaksysteem::Tools qw(elfproef);

my $bsn = generate_bsn();
ok(elfproef($bsn, 1), "generate_bsn ($bsn) is BSN elfproef");

my $rsin = generate_rsin();
ok(elfproef($rsin), "generate_rsin ($rsin) is elfproef");

my $kvk = generate_kvk();
ok(elfproef($kvk), "generate_kvk ($kvk) is elfproef");

my $uuid = generate_uuid_v4();
my $section = qr/[0-9a-f]/;
my $uuid_re = qr/^$section{8}-?$section{4}-?4$section{3}-?[89ab]$section{3}-?$section{12}$/;
like($uuid, $uuid_re, "Looks like a UUID v4");

done_testing;
